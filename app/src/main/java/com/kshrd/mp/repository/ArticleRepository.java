package com.kshrd.mp.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.mp.data.model.Article;
import com.kshrd.mp.data.model.response.ArticleResponse;
import com.kshrd.mp.data.model.response.DeleteResponse;
import com.kshrd.mp.data.model.response.ImageResponse;
import com.kshrd.mp.data.model.UploadArticle;
import com.kshrd.mp.data.model.response.UpdateResponse;
import com.kshrd.mp.data.remote.ArticleService;
import com.kshrd.mp.data.remote.APIService;

import org.jetbrains.annotations.NotNull;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private final ArticleService service;

    public ArticleRepository() {
        this.service = APIService.createService(ArticleService.class);
    }

    public MutableLiveData<ArticleResponse> findArticles() {

        final MutableLiveData<ArticleResponse> mutableLiveData = new MutableLiveData<>();

        Call<ArticleResponse> articles = service.findArticles();

        articles.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(@NotNull Call<ArticleResponse> call, @NotNull Response<ArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("findArticles", "isSuccessful : " + response.body().getMessage());
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ArticleResponse> call, @NotNull Throwable t) {
                Log.d("findArticles", "onFailure : " + t.getMessage());
            }
        });

        return mutableLiveData;

    }

    public MutableLiveData<DeleteResponse> deleteArticle(int id) {

        final MutableLiveData<DeleteResponse> mutableLiveData = new MutableLiveData<>();

        Call<DeleteResponse> call = service.deleteArticle(id);

        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(@NotNull Call<DeleteResponse> call, @NotNull Response<DeleteResponse> response) {
                Log.d("DeleteArticle", "isSuccessful");
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<DeleteResponse> call, @NotNull Throwable t) {
                Log.d("DeleteArticle", "onFailure" + t.getMessage());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<UpdateResponse> updateArticle(int id, UploadArticle uploadArticle) {

        final MutableLiveData<UpdateResponse> mutableLiveData = new MutableLiveData<>();

        Call<UpdateResponse> updateResponseCall = service.updateArticle(id, uploadArticle);

        updateResponseCall.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(@NotNull Call<UpdateResponse> call, @NotNull Response<UpdateResponse> response) {
                Log.d("updateArticle", "isSuccessful");
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<UpdateResponse> call, @NotNull Throwable t) {
                Log.d("updateArticle", "onFailure" + t.getMessage());
            }
        });
        return mutableLiveData;
    }

    public MutableLiveData<UploadArticle> uploadArticle(UploadArticle uploadArticle) {

        final MutableLiveData<UploadArticle> mutableLiveData = new MutableLiveData<>();

        Call<UploadArticle> uploadArticleCall = service.uploadArticle(uploadArticle);

        uploadArticleCall.enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                Log.d("uploadArticle", "isSuccessful");
                mutableLiveData.setValue((UploadArticle) response.body());
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull Throwable t) {
                Log.d("uploadArticle", "onFailure : " + t.getMessage());
            }
        });

        return mutableLiveData;
    }

    public MutableLiveData<Article> findArticleById(int id) {

        final MutableLiveData<Article> mutableLiveData = new MutableLiveData<>();

        Call<Article> findArticleCall = service.findArticleById(id);

        findArticleCall.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(@NotNull Call<Article> call, @NotNull Response<Article> response) {
                Log.d("findArticleById", "isSuccessful");
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<Article> call, @NotNull Throwable t) {
                Log.d("findArticleById", "onFailure : " + t.getMessage());
            }
        });

        return mutableLiveData;
    }

    public MutableLiveData<ImageResponse> getImage(RequestBody file, MultipartBody.Part image) {

        final MutableLiveData<ImageResponse> responseMutableLiveData = new MutableLiveData<>();

        Call<ImageResponse> uploadImage = service.updateProfile(file, image);

        uploadImage.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(@NotNull Call<ImageResponse> call, @NotNull Response<ImageResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("getImage", "isSuccessful : " + response.body().getMessage());
                    responseMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ImageResponse> call, @NotNull Throwable t) {
                Log.d("getImage", "onFailure : " + t.getMessage());
            }
        });

        return responseMutableLiveData;
    }

}

