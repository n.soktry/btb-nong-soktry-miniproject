package com.kshrd.mp.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.kshrd.mp.R;
import com.kshrd.mp.data.model.Article;
import com.kshrd.mp.data.model.response.ArticleResponse;
import com.kshrd.mp.data.model.response.DeleteResponse;
import com.kshrd.mp.view.adapter.ArticleAdapter;
import com.kshrd.mp.view.articleDetail.DetailActivity;
import com.kshrd.mp.view.articleUpdate.UpdateActivity;
import com.kshrd.mp.view.articleUpload.UploadActivity;
import com.kshrd.mp.viewmodel.ViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ArticleAdapter.GetArticle {

    public FloatingActionButton fabButton;
    ArticleResponse articleResponse;
    ArticleAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ViewModel viewModel;
    LinearLayoutManager layoutManager;
    CoordinatorLayout coordinatorLayout;
    RecyclerView recyclerView;
    List<Article> articles;

    float dX;
    float dY;
    int lastAction;

    private CountDownTimer countDownTimer;
    private long millisTime;
    final boolean[] isDelete = {false};

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        fabButton = findViewById(R.id.home_button_add);
        coordinatorLayout = findViewById(R.id.container_coordinator);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), UploadActivity.class);

        //on touch listener for movable fab button
        fabButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        dX = view.getX() - motionEvent.getRawX();
                        dY = view.getY() - motionEvent.getRawY();
                        lastAction = MotionEvent.ACTION_DOWN;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        view.setY(motionEvent.getRawY() + dY);
                        view.setX(motionEvent.getRawX() + dX);
                        lastAction = MotionEvent.ACTION_MOVE;
                        break;

                    case MotionEvent.ACTION_UP:
                        if (lastAction == MotionEvent.ACTION_DOWN)
                            startActivity(intent);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        //on recycler scroll listener to hide fab button
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fabButton.isShown())
                    fabButton.hide();
            }

            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    fabButton.show();
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        viewModel = ViewModelProviders.of(MainActivity.this).get(ViewModel.class);
        viewModel.init();

        getLiveArticles();
        swipeRefresh();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.top) {
            Toast.makeText(getApplicationContext(), "Moving to the top!", Toast.LENGTH_SHORT).show();
            recyclerView.smoothScrollToPosition(0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpRecyclerView() {
        if (adapter == null) {
            adapter = new ArticleAdapter(getApplicationContext(), articleResponse.getArticles(), this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
            articles = adapter.articles();
        } else {
            adapter.setArticles(articleResponse.getArticles());
            adapter.notifyDataSetChanged();
        }
    }

    private void getLiveArticles() {
        viewModel.getFindArticles().observe(MainActivity.this, new Observer<ArticleResponse>() {
            @Override
            public void onChanged(ArticleResponse articleResponse) {
                MainActivity.this.articleResponse = articleResponse;
                setUpRecyclerView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void swipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLiveArticles();
                viewModel.getArticles();
            }
        });
    }

    @Override
    public void onItemClick(Article article) {
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        intent.putExtra("detail", article);
        startActivity(intent);
    }

    @Override
    public void onItemUpdate(Article article) {
        Intent intent = new Intent(getApplicationContext(), UpdateActivity.class);
        intent.putExtra("update", article);
        startActivity(intent);
    }

    @Override
    public void onItemDelete(Article article) {

        deleteArticle(article);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "An article is deleted", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbarOne = Snackbar.make(coordinatorLayout, "An article is back!", Snackbar.LENGTH_SHORT);
                        isDelete[0] = true;
                        snackbarOne.show();
                    }
                });

        snackbar.show();
    }

    private void deleteArticle(final Article article) {
        countDownTimer = new CountDownTimer(3000, 3000) {
            @Override
            public void onTick(long millisUntilFinished) {
                millisTime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                if (isDelete[0]) {
                    isDelete[0] = false;
                } else {
                    viewModel.setId(article.getId());
                    viewModel.deleteArticle();
                    viewModel.getDeleteArticle().observe(MainActivity.this, new Observer<DeleteResponse>() {
                        @Override
                        public void onChanged(DeleteResponse deleteResponse) {
                            viewModel.getArticles();
                            getLiveArticles();
                        }
                    });
                }
            }
        }.start();
    }

}