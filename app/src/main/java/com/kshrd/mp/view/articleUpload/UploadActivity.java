package com.kshrd.mp.view.articleUpload;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.kshrd.mp.R;
import com.kshrd.mp.data.model.response.ImageResponse;
import com.kshrd.mp.data.model.UploadArticle;
import com.kshrd.mp.view.MainActivity;
import com.kshrd.mp.viewmodel.ViewModel;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadActivity extends AppCompatActivity {

    ImageView image;
    EditText title, desc, date;
    Button save, cancel;
    ImageResponse imageResponse = null;
    ViewModel viewModel;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        viewModel = ViewModelProviders.of(this).get(ViewModel.class);
        viewModel.init();


        image = findViewById(R.id.upload_image);
        title = findViewById(R.id.upload_title);
        date = findViewById(R.id.upload_date);
        desc = findViewById(R.id.upload_desc);
        save = findViewById(R.id.save_button);
        cancel = findViewById(R.id.cancel_button);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setTitle = title.getText().toString();
                String setDesc = UploadActivity.this.desc.getText().toString();
                if (!setTitle.isEmpty() && !setDesc.isEmpty()) {

                    UploadArticle uploadArticle = new UploadArticle();
                    uploadArticle.setDesc(setDesc);
                    uploadArticle.setTitle(setTitle);

                    if (imageResponse != null) {
                        uploadArticle.setImage(imageResponse.getImage());

                        viewModel.uploadArticle(uploadArticle);
                        viewModel.uploadArticle();
                        viewModel.getUploadArticle().observe(UploadActivity.this, new Observer<UploadArticle>() {
                            @Override
                            public void onChanged(UploadArticle uploadArticle) {

                            }
                        });
                        Intent intent = new Intent(UploadActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else if (imageResponse == null) {
                        Toast.makeText(getApplicationContext(), "Please add an image and \nwait for a moment!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please wait till image is uploaded successfully!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please input title and desc correctly!", Toast.LENGTH_LONG).show();
                }

            }
        });


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intentImage = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentImage, 1);
                } catch (Exception ignored) {
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Date currentTime = Calendar.getInstance().getTime();
        date.setText(currentTime.toString());
        date.setEnabled(false);
        requestStoragePermission();
    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Permission granted!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Permission denied!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            uri = data.getData();
            image.setImageURI(uri);

            File file = new File(getPath(uri));

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

            viewModel.setFile(file);
            viewModel.setFileBody(requestBody);
            viewModel.setImage(body);
            viewModel.uploadImage();
            viewModel.getUploadImage().observe(this, new Observer<ImageResponse>() {
                @Override
                public void onChanged(ImageResponse imageResponse) {
                    UploadActivity.this.imageResponse = imageResponse;
                    Toast.makeText(UploadActivity.this, "Uploaded image successfully!", Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

}
