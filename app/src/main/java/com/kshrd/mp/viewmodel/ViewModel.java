package com.kshrd.mp.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.mp.data.model.Article;
import com.kshrd.mp.data.model.response.ArticleResponse;
import com.kshrd.mp.data.model.response.DeleteResponse;
import com.kshrd.mp.data.model.response.ImageResponse;
import com.kshrd.mp.data.model.UploadArticle;
import com.kshrd.mp.data.model.response.UpdateResponse;
import com.kshrd.mp.repository.ArticleRepository;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ViewModel extends androidx.lifecycle.ViewModel {

    private MutableLiveData<ArticleResponse> FindArticles;
    private MutableLiveData<ImageResponse> uploadImage;
    private MutableLiveData<UploadArticle> uploadArticle;
    private MutableLiveData<DeleteResponse> deleteArticle;
    private MutableLiveData<UpdateResponse> updateArticle;
    private MutableLiveData<Article> findArticleById;

    private ArticleRepository articleRepository;
    File file;
    int id;
    UploadArticle uploadArticles;

    RequestBody fileBody;
    MultipartBody.Part image;

    public void init() {
        articleRepository = new ArticleRepository();
        FindArticles = articleRepository.findArticles();
    }

    public void getArticles() {
        FindArticles = articleRepository.findArticles();
    }

    public void getArticleById() {
        findArticleById = articleRepository.findArticleById(id);
    }

    public void deleteArticle() {
        deleteArticle = articleRepository.deleteArticle(id);
    }

    public void uploadArticle() {
        uploadArticle = articleRepository.uploadArticle(uploadArticles);
    }

    public void uploadImage() {
        uploadImage = articleRepository.getImage(fileBody, image);
    }

    public void updateArticle() {
        updateArticle = articleRepository.updateArticle(id, uploadArticles);
    }

    public MutableLiveData<UpdateResponse> getUpdateArticle() {
        return updateArticle;
    }

    public void setFileBody(RequestBody fileBody) {
        this.fileBody = fileBody;
    }

    public void setImage(MultipartBody.Part image) {
        this.image = image;
    }

    public MutableLiveData<ArticleResponse> getFindArticles() {
        return FindArticles;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void uploadArticle(UploadArticle uploadArticle) {
        this.uploadArticles = uploadArticle;
    }

    public MutableLiveData<Article> getFindArticleById() {
        return findArticleById;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MutableLiveData<DeleteResponse> getDeleteArticle() {
        return deleteArticle;
    }

    public MutableLiveData<UploadArticle> getUploadArticle() {
        return uploadArticle;
    }

    public MutableLiveData<ImageResponse> getUploadImage() {
        return uploadImage;
    }


}

