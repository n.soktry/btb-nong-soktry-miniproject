package com.kshrd.mp.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.mp.data.model.Article;

public class DeleteResponse {

    @SerializedName("data")
    private Article data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public DeleteResponse() {
    }

    public DeleteResponse(Article data, String message, int code) {
        this.data = data;
        this.message = message;
        this.code = code;
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
