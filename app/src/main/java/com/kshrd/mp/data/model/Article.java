package com.kshrd.mp.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Article implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("description")
    private String desc;
    @SerializedName("image_url")
    private String imageUrl;

    @Override
    public String toString() {
        return "Article{" +
                "imageUrl='" + imageUrl + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                ", description='" + desc + '\'' +
                '}';
    }

    public Article() {
    }

    public Article(String imageUrl, String createdDate, String title, int id, String desc) {
        this.imageUrl = imageUrl;
        this.createdDate = createdDate;
        this.title = title;
        this.id = id;
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

