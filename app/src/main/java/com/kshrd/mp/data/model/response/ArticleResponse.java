package com.kshrd.mp.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.mp.data.model.Article;

import java.util.List;

public class ArticleResponse {

    @SerializedName("data")
    private List<Article> data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public List<Article> getArticles() {
        return data;
    }

    public void setData(List<Article> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ListArticleResponse{" +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}
