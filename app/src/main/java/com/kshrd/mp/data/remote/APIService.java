package com.kshrd.mp.data.remote;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIService {

    private static final String BASE_URL = "http://110.74.194.124:15011/v1/api/";
    private static final String API_KEY = "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";

    private static final Retrofit.Builder builder = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL);
    private static final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

    public static <S> S createService(Class<S> serviceClass) {
        okHttpBuilder.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder builder = request.newBuilder()
                        .addHeader("Authorization", API_KEY)
                        .header("Accept", "application/json")
                        .method(request.method(), request.body());
                return chain.proceed(builder.build());
            }
        });
        return builder.client(okHttpBuilder.build()).build().create(serviceClass);
    }
}
