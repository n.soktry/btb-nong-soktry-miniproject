package com.kshrd.mp.data.remote;

import com.kshrd.mp.data.model.Article;
import com.kshrd.mp.data.model.response.ArticleResponse;
import com.kshrd.mp.data.model.response.DeleteResponse;
import com.kshrd.mp.data.model.response.ImageResponse;
import com.kshrd.mp.data.model.UploadArticle;
import com.kshrd.mp.data.model.response.UpdateResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("articles")
    Call<ArticleResponse> findArticles();

    @GET("articles/{id}")
    Call<Article> findArticleById(@Path("id") int id);

    @DELETE("articles/{id}")
    Call<DeleteResponse> deleteArticle(@Path("id") int id);

    @POST("articles")
    Call<UploadArticle> uploadArticle(@Body UploadArticle uploadArticle);

    @Multipart
    @POST("uploadfile/single")
    Call<ImageResponse> updateProfile(@Part("file") RequestBody file, @Part MultipartBody.Part image);

    @PUT("articles/{id}")
    Call<UpdateResponse> updateArticle(@Path("id") int id, @Body UploadArticle uploadArticle);

}
