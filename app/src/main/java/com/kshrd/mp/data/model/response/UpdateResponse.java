package com.kshrd.mp.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.mp.data.model.Article;

public class UpdateResponse {

    @SerializedName("data")
    private Article article;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    @Override
    public String toString() {
        return "UpdateResponse{" +
                ", data=" + article +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }

    public UpdateResponse() {
    }

    public UpdateResponse(Article article, String message, int code) {
        this.article = article;
        this.message = message;
        this.code = code;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
